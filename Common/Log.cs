using System;
using System.IO;

namespace Common
{
    /// <summary>
    /// Log 的摘要说明。
    /// </summary>
    public class Log
    {
        private string logFilePath;
        /// <summary>
        /// 设置日志保存文件
        /// </summary>
        public string FilePath
        {
            set
            {
                this.logFilePath = value;
            }
            get { return this.logFilePath; }
        }

        private int logFileLength;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="filePath">日志文件完整路径(默认当前路径)</param>
        /// <param name="fileLength">日志文件大小(默认大小5M)</param>
        public Log(string filePath, int fileLength)
        {
            this.logFilePath = filePath;
            this.logFileLength = fileLength;
        }

        private void CheckLogFileSize()
        {
            //获取设置的日志文件路径和大小
            int flength = this.logFileLength;
            if (flength == 0)
                flength = 5242880;
            //指定日志文件的目录
            string fname = this.logFilePath;
            if (string.IsNullOrEmpty(fname))
            {
                fname = "Log_" + System.DateTime.Now.ToShortDateString() + ".log";
                this.logFilePath = fname;
            }

            //定义文件信息对象
            FileInfo finfo = new FileInfo(fname);

                //判断文件是否存在以及是否大于1m
                if (finfo.Exists && finfo.Length > flength)
                {
                    //新建文件
                    string fTmp = Path.Combine(Path.GetDirectoryName(fname), "Log_" + System.DateTime.Now.ToString("yyyyMMddHHmmss") + ".log");
                    this.logFilePath = fTmp;
                }
        }

        /// <summary>
        /// 记录日志
        /// </summary>
        /// <param name="input">记录的日志内容</param>
        public void WriteLogFile(string input)
        {
            try
            {
                this.CheckLogFileSize();

                //指定日志文件的目录
                string fname = this.logFilePath;

                if (fname != "" && !Directory.Exists(Path.GetDirectoryName(fname)))
                    Directory.CreateDirectory(Path.GetDirectoryName(fname));

                //定义文件信息对象
                FileInfo finfo = new FileInfo(fname);
                    //创建只写文件流
                    using (FileStream fs = finfo.OpenWrite())
                    {
                        //根据上面创建的文件流创建写数据流
                        StreamWriter w = new StreamWriter(fs, System.Text.Encoding.Default);
                        //设置写数据流的起始位置为文件流的末尾
                        w.BaseStream.Seek(0, SeekOrigin.End);
                        //写入“Log Entry : ”
                        //w.Write("Log Entry : ");
                        //写入当前系统时间并换行
                        w.Write("[{0} {1}] ", DateTime.Now.ToShortDateString(),
                            DateTime.Now.ToLongTimeString());
                        //写入日志内容并换行
                        w.Write(input + "\r\n");
                        //清空缓冲区内容，并把缓冲区内容写入基础流
                        w.Flush();
                        //关闭写数据流
                        w.Close();
                        fs.Close();

                    }
            }
            catch
            {

            }
        }

        /// <summary>
        /// 写文件
        /// </summary>
        /// <param name="input">记录文件内容</param>
        public void WriteFile(string input)
        {
            //指定文件的目录
            string fname = this.logFilePath;

            //定义文件信息对象
            FileInfo finfo = new FileInfo(fname);

            //创建只写文件流
            using (FileStream fs = finfo.OpenWrite())
            {
                //根据上面创建的文件流创建写数据流
                StreamWriter w = new StreamWriter(fs);
                //设置写数据流的起始位置为文件流的末尾
                w.BaseStream.Seek(0, SeekOrigin.End);
                //写入文件内容并换行
                w.Write(input + "\r\n");
                //清空缓冲区内容，并把缓冲区内容写入基础流
                w.Flush();
                //关闭写数据流
                w.Close();
                fs.Close();
            }
            if (finfo != null)
                finfo = null;
        }
    }
}