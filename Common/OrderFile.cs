﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common
{
    /// <summary>
    /// 文件排序类
    /// </summary>
    public class OrderFile
    {
        string fileName = "";
        int fileSize = 0;
        DateTime fileDate;

        /// <summary>
        /// 文件名
        /// </summary>
        public string FileName
        {
            set { this.fileName = value; }
            get { return this.fileName; }
        }

        /// <summary>
        /// 文件大小
        /// </summary>
        public int FileSize
        {
            set { this.fileSize = value; }
            get { return this.fileSize; }
        }

        /// <summary>
        /// 文件最后修改时间
        /// </summary>
        public DateTime FileDate
        {
            set { this.fileDate = value; }
            get { return this.fileDate; }
        }

        /// <summary>
        /// 排序FtpFiles
        /// </summary>
        /// <param name="strs">LIST后的文件详细信息列表</param>
        /// <returns></returns>
        public static OrderFile[] GetOrderFtpFiles(string[] strs)
        {
            OrderFile[] res = null;

            List<OrderFile> list = new List<OrderFile>();

                OrderFile file = null;
                foreach (string str in strs)
                {
                    file = new OrderFile();
                    file.FileName = str;
                    list.Add(file);
                }
                if (list.Count > 0)
                {
                    CompareFileInfo cf = new CompareFileInfo();
                    list.Sort(cf.Compare);
                    res = list.ToArray();
                }
            return res;
        }

        /// <summary>
        /// 排序Files
        /// </summary>
        /// <param name="strs">带路径File</param>
        /// <returns></returns>
        public static string[] GetOrderFiles(string[] strs)
        {
            string[] res = null;

            List<string> list = new List<string>();
                foreach (string str in strs)
                {
                    list.Add(str);
                }
                if (list.Count > 0)
                {
                    CompareString cf = new CompareString();
                    list.Sort(cf.Compare);
                    res = list.ToArray();
                }
            return res;
        }
    }

    public class CompareFileInfo : System.Collections.IComparer
    {
        /// <summary>
        /// 文件排序方法
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(Object x, Object y)
        {
            OrderFile file1 = (OrderFile)x;
            OrderFile file2 = (OrderFile)y;

            return String.Compare(file1.FileName, file2.FileName);
        }
    }

    public class CompareString : System.Collections.IComparer
    {
        public int Compare(Object x, Object y)
        {
            return String.Compare((string)x, (string)y);
        }
    }
}
