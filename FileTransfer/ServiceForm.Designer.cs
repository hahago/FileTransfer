﻿namespace FileTransfer
{
    partial class ServiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbServiceName = new System.Windows.Forms.TextBox();
            this.tbServiceDesc = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSeviceSave = new System.Windows.Forms.Button();
            this.btnServiceInstall = new System.Windows.Forms.Button();
            this.btnServiceUninstall = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "服务名称";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "服务描述";
            // 
            // tbServiceName
            // 
            this.tbServiceName.Location = new System.Drawing.Point(80, 6);
            this.tbServiceName.Name = "tbServiceName";
            this.tbServiceName.Size = new System.Drawing.Size(173, 21);
            this.tbServiceName.TabIndex = 2;
            // 
            // tbServiceDesc
            // 
            this.tbServiceDesc.Location = new System.Drawing.Point(80, 33);
            this.tbServiceDesc.Name = "tbServiceDesc";
            this.tbServiceDesc.Size = new System.Drawing.Size(173, 21);
            this.tbServiceDesc.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "服务操作";
            // 
            // btnSeviceSave
            // 
            this.btnSeviceSave.Location = new System.Drawing.Point(261, 32);
            this.btnSeviceSave.Name = "btnSeviceSave";
            this.btnSeviceSave.Size = new System.Drawing.Size(66, 23);
            this.btnSeviceSave.TabIndex = 5;
            this.btnSeviceSave.Text = "保 存";
            this.btnSeviceSave.UseVisualStyleBackColor = true;
            this.btnSeviceSave.Click += new System.EventHandler(this.btnSeviceSave_Click);
            // 
            // btnServiceInstall
            // 
            this.btnServiceInstall.Location = new System.Drawing.Point(80, 61);
            this.btnServiceInstall.Name = "btnServiceInstall";
            this.btnServiceInstall.Size = new System.Drawing.Size(66, 23);
            this.btnServiceInstall.TabIndex = 6;
            this.btnServiceInstall.Text = "安装服务";
            this.btnServiceInstall.UseVisualStyleBackColor = true;
            this.btnServiceInstall.Click += new System.EventHandler(this.btnServiceInstall_Click);
            // 
            // btnServiceUninstall
            // 
            this.btnServiceUninstall.Location = new System.Drawing.Point(167, 61);
            this.btnServiceUninstall.Name = "btnServiceUninstall";
            this.btnServiceUninstall.Size = new System.Drawing.Size(66, 23);
            this.btnServiceUninstall.TabIndex = 7;
            this.btnServiceUninstall.Text = "卸载服务";
            this.btnServiceUninstall.UseVisualStyleBackColor = true;
            this.btnServiceUninstall.Click += new System.EventHandler(this.btnServiceUninstall_Click);
            // 
            // ServiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 93);
            this.Controls.Add(this.btnServiceUninstall);
            this.Controls.Add(this.btnServiceInstall);
            this.Controls.Add(this.btnSeviceSave);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbServiceDesc);
            this.Controls.Add(this.tbServiceName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ServiceForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "服务设置";
            this.Load += new System.EventHandler(this.ServiceForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbServiceName;
        private System.Windows.Forms.TextBox tbServiceDesc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSeviceSave;
        private System.Windows.Forms.Button btnServiceInstall;
        private System.Windows.Forms.Button btnServiceUninstall;
    }
}