﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceProcess;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Config;

namespace FileTransfer
{
    public partial class ServiceForm : Form
    {
        ServiceController sc = null;

        public ServiceForm()
        {
            InitializeComponent();
        }

        #region 初始化
        private void ServiceForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.tbServiceName.Text = SystemSet.AppServiceName;
                this.tbServiceDesc.Text = SystemSet.AppServiceDesc;
                SetServiceStatus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "初始化错误！错误原因：" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region 按钮操作
        private void btnSeviceSave_Click(object sender, EventArgs e)
        {
            try
            {
                SystemSet.AppServiceName = this.tbServiceName.Text.Trim();
                SystemSet.AppServiceDesc = this.tbServiceDesc.Text.Trim();
                SystemSet.ClearCache();
                SetServiceStatus();
                MessageBox.Show("保存成功！");
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "操作出错！错误原因：" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        private void btnServiceInstall_Click(object sender, EventArgs e)
        {
            string installBatFile = "";
            try
            {
                if (string.IsNullOrEmpty(SystemSet.AppServiceName))
                {
                    MessageBox.Show(this, "服务名称不能为空！", "警示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.tbServiceName.Focus();
                }
                installBatFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "InstallService.bat");
                if (System.IO.File.Exists(installBatFile))
                {
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    process.StartInfo.FileName = installBatFile;
                    process.StartInfo.UseShellExecute = true;
                    process.Start();
                    process.WaitForExit();
                    process.Dispose();

                    this.SetServiceStatus();
                }
                else
                    MessageBox.Show(this, "系统服务安装文件InstallService.bat不存在，请检查系统是否安装正确！", "警示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "操作出错！错误原因：" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnServiceUninstall_Click(object sender, EventArgs e)
        {
            string batFile = "";
            try
            {
                batFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "UninstallService.bat");
                if (System.IO.File.Exists(batFile))
                {
                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    process.StartInfo.FileName = batFile;
                    process.StartInfo.UseShellExecute = true;
                    process.Start();
                    process.WaitForExit();
                    process.Dispose();

                    this.SetServiceStatus();
                }
                else
                    MessageBox.Show(this, "系统服务卸载文件UninstallService.bat不存在，请检查系统是否安装正确！", "警示", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "操作出错！错误原因：" + ex.Message, "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region 公共方法
        /// <summary>
        /// 获取服务状态
        /// </summary>
        private void SetServiceStatus()
        {
            ServiceController[] scServices = ServiceController.GetServices();
            this.sc = null;
            foreach (ServiceController scTemp in scServices)
            {
                if (scTemp.ServiceName == SystemSet.AppServiceName)
                {
                    this.sc = scTemp;
                    this.btnServiceInstall.Enabled = false;
                    this.btnServiceUninstall.Enabled = true;

                    break;
                }   
            }
            if (this.sc == null)
            {
                this.btnServiceInstall.Enabled = true;
                this.btnServiceUninstall.Enabled = false;
            }
        }
        #endregion
    }
}
