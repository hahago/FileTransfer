namespace FileTransfer
{
    partial class TaskForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbTaskNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSendStop = new System.Windows.Forms.Button();
            this.btnSendStart = new System.Windows.Forms.Button();
            this.btnGetStop = new System.Windows.Forms.Button();
            this.btnGetStart = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbGet = new System.Windows.Forms.GroupBox();
            this.pnActiveMqGet = new System.Windows.Forms.Panel();
            this.tbActiveMqGetDestinationName = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.chbActiveMqGetTranscation = new System.Windows.Forms.CheckBox();
            this.tbActiveMqGetConnectName = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.tbGetThreadTime = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.pnMsmqGet = new System.Windows.Forms.Panel();
            this.tbMsmqGetEncoding = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.cbMsmqGetFileType = new System.Windows.Forms.ComboBox();
            this.chbMsmqGetTranscation = new System.Windows.Forms.CheckBox();
            this.tbMsmqGetQueueName = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbActiveMqGet = new System.Windows.Forms.RadioButton();
            this.rbMsmqGet = new System.Windows.Forms.RadioButton();
            this.rbLocal = new System.Windows.Forms.RadioButton();
            this.rbFtpDown = new System.Windows.Forms.RadioButton();
            this.rbEmailGet = new System.Windows.Forms.RadioButton();
            this.label35 = new System.Windows.Forms.Label();
            this.tbGetSaveTimes = new System.Windows.Forms.TextBox();
            this.btnGetRoot = new System.Windows.Forms.Button();
            this.tbGetMessageRoot = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.pnEmailRec = new System.Windows.Forms.Panel();
            this.tbEmailRecUserPass = new System.Windows.Forms.TextBox();
            this.tbEmailRecUser = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbEmailRecServer = new System.Windows.Forms.TextBox();
            this.tbEmailRecPort = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pnFtpDown = new System.Windows.Forms.Panel();
            this.tbFtpDownEncoding = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.chbFtpDownFileRename = new System.Windows.Forms.CheckBox();
            this.tbFtpDownFileEx = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chbFtpDownMode = new System.Windows.Forms.CheckBox();
            this.tbFtpDownUserPass = new System.Windows.Forms.TextBox();
            this.tbFtpDownUser = new System.Windows.Forms.TextBox();
            this.tbFtpDownServer = new System.Windows.Forms.TextBox();
            this.tbFtpDownRoot = new System.Windows.Forms.TextBox();
            this.tbFtpDownPort = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnLocal = new System.Windows.Forms.Panel();
            this.tbLoaclRootFileEx = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.btnRootView = new System.Windows.Forms.Button();
            this.tbLoaclRoot = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbSend = new System.Windows.Forms.GroupBox();
            this.pnActiveMqSend = new System.Windows.Forms.Panel();
            this.tbActiveMqSendDestinationName = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.chbActiveMqSendTranscation = new System.Windows.Forms.CheckBox();
            this.tbActiveMqSendConnectName = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.tbSendThreadTime = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tbSendFileEx = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.pnMsmqSend = new System.Windows.Forms.Panel();
            this.tbMsmqSendEncoding = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.cbMsmqSendFileType = new System.Windows.Forms.ComboBox();
            this.chbMsmqSendTranscation = new System.Windows.Forms.CheckBox();
            this.tbMsmqSendQueueName = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbSendCorpNo = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnSendRoot = new System.Windows.Forms.Button();
            this.tbSendMessageRoot = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbActiveMqSend = new System.Windows.Forms.RadioButton();
            this.rbMsmqSend = new System.Windows.Forms.RadioButton();
            this.rbEmailSend = new System.Windows.Forms.RadioButton();
            this.rbFtpUpload = new System.Windows.Forms.RadioButton();
            this.pnEmailSend = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.tbEmailSendTo = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbEmailSendUserPass = new System.Windows.Forms.TextBox();
            this.tbEmailSendUser = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tbEmailSendServer = new System.Windows.Forms.TextBox();
            this.tbEmailSendPort = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.pnFtpUpload = new System.Windows.Forms.Panel();
            this.tbFtpUploadEncoding = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chbFtpUploadMode = new System.Windows.Forms.CheckBox();
            this.tbFtpUploadUserPass = new System.Windows.Forms.TextBox();
            this.tbFtpUploadUser = new System.Windows.Forms.TextBox();
            this.tbFtpUploadServer = new System.Windows.Forms.TextBox();
            this.tbFtpUploadRoot = new System.Windows.Forms.TextBox();
            this.tbFtpUploadPort = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            this.fbdGetRoot = new System.Windows.Forms.FolderBrowserDialog();
            this.fdbSendRoot = new System.Windows.Forms.FolderBrowserDialog();
            this.fbdTransferFrom = new System.Windows.Forms.FolderBrowserDialog();
            this.fbdTransferTo = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpGet = new System.Windows.Forms.TabPage();
            this.tpSend = new System.Windows.Forms.TabPage();
            this.gbGet.SuspendLayout();
            this.pnActiveMqGet.SuspendLayout();
            this.pnMsmqGet.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnEmailRec.SuspendLayout();
            this.pnFtpDown.SuspendLayout();
            this.pnLocal.SuspendLayout();
            this.gbSend.SuspendLayout();
            this.pnActiveMqSend.SuspendLayout();
            this.pnMsmqSend.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnEmailSend.SuspendLayout();
            this.pnFtpUpload.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tpGet.SuspendLayout();
            this.tpSend.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbTaskNo
            // 
            this.tbTaskNo.Location = new System.Drawing.Point(94, 16);
            this.tbTaskNo.Name = "tbTaskNo";
            this.tbTaskNo.Size = new System.Drawing.Size(101, 21);
            this.tbTaskNo.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(34, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "任务代码";
            // 
            // btnSendStop
            // 
            this.btnSendStop.Location = new System.Drawing.Point(307, 15);
            this.btnSendStop.Name = "btnSendStop";
            this.btnSendStop.Size = new System.Drawing.Size(41, 23);
            this.btnSendStop.TabIndex = 5;
            this.btnSendStop.Text = "关闭";
            this.btnSendStop.UseVisualStyleBackColor = true;
            this.btnSendStop.Click += new System.EventHandler(this.btnSendStart_Click);
            // 
            // btnSendStart
            // 
            this.btnSendStart.Location = new System.Drawing.Point(260, 15);
            this.btnSendStart.Name = "btnSendStart";
            this.btnSendStart.Size = new System.Drawing.Size(41, 23);
            this.btnSendStart.TabIndex = 4;
            this.btnSendStart.Text = "开启";
            this.btnSendStart.UseVisualStyleBackColor = true;
            this.btnSendStart.Click += new System.EventHandler(this.btnSendStart_Click);
            // 
            // btnGetStop
            // 
            this.btnGetStop.Location = new System.Drawing.Point(307, 14);
            this.btnGetStop.Name = "btnGetStop";
            this.btnGetStop.Size = new System.Drawing.Size(41, 23);
            this.btnGetStop.TabIndex = 3;
            this.btnGetStop.Text = "关闭";
            this.btnGetStop.UseVisualStyleBackColor = true;
            this.btnGetStop.Click += new System.EventHandler(this.btnGetStart_Click);
            // 
            // btnGetStart
            // 
            this.btnGetStart.Location = new System.Drawing.Point(260, 14);
            this.btnGetStart.Name = "btnGetStart";
            this.btnGetStart.Size = new System.Drawing.Size(41, 23);
            this.btnGetStart.TabIndex = 2;
            this.btnGetStart.Text = "开启";
            this.btnGetStart.UseVisualStyleBackColor = true;
            this.btnGetStart.Click += new System.EventHandler(this.btnGetStart_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(203, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "任务控制";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(202, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "任务控制";
            // 
            // gbGet
            // 
            this.gbGet.Controls.Add(this.pnActiveMqGet);
            this.gbGet.Controls.Add(this.label41);
            this.gbGet.Controls.Add(this.tbGetThreadTime);
            this.gbGet.Controls.Add(this.label40);
            this.gbGet.Controls.Add(this.pnMsmqGet);
            this.gbGet.Controls.Add(this.panel2);
            this.gbGet.Controls.Add(this.label35);
            this.gbGet.Controls.Add(this.tbGetSaveTimes);
            this.gbGet.Controls.Add(this.tbTaskNo);
            this.gbGet.Controls.Add(this.label3);
            this.gbGet.Controls.Add(this.btnGetRoot);
            this.gbGet.Controls.Add(this.btnGetStop);
            this.gbGet.Controls.Add(this.tbGetMessageRoot);
            this.gbGet.Controls.Add(this.btnGetStart);
            this.gbGet.Controls.Add(this.label25);
            this.gbGet.Controls.Add(this.label1);
            this.gbGet.Controls.Add(this.pnEmailRec);
            this.gbGet.Controls.Add(this.pnFtpDown);
            this.gbGet.Controls.Add(this.pnLocal);
            this.gbGet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbGet.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbGet.Location = new System.Drawing.Point(3, 3);
            this.gbGet.Name = "gbGet";
            this.gbGet.Size = new System.Drawing.Size(483, 374);
            this.gbGet.TabIndex = 6;
            this.gbGet.TabStop = false;
            // 
            // pnActiveMqGet
            // 
            this.pnActiveMqGet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnActiveMqGet.Controls.Add(this.tbActiveMqGetDestinationName);
            this.pnActiveMqGet.Controls.Add(this.label47);
            this.pnActiveMqGet.Controls.Add(this.chbActiveMqGetTranscation);
            this.pnActiveMqGet.Controls.Add(this.tbActiveMqGetConnectName);
            this.pnActiveMqGet.Controls.Add(this.label46);
            this.pnActiveMqGet.Location = new System.Drawing.Point(94, 331);
            this.pnActiveMqGet.Name = "pnActiveMqGet";
            this.pnActiveMqGet.Size = new System.Drawing.Size(380, 36);
            this.pnActiveMqGet.TabIndex = 18;
            // 
            // tbActiveMqGetDestinationName
            // 
            this.tbActiveMqGetDestinationName.Location = new System.Drawing.Point(235, 7);
            this.tbActiveMqGetDestinationName.Name = "tbActiveMqGetDestinationName";
            this.tbActiveMqGetDestinationName.Size = new System.Drawing.Size(75, 21);
            this.tbActiveMqGetDestinationName.TabIndex = 11;
            this.tbActiveMqGetDestinationName.Text = "queue://test";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(192, 12);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(41, 12);
            this.label47.TabIndex = 10;
            this.label47.Text = "连接名";
            // 
            // chbActiveMqGetTranscation
            // 
            this.chbActiveMqGetTranscation.AutoSize = true;
            this.chbActiveMqGetTranscation.Location = new System.Drawing.Point(324, 10);
            this.chbActiveMqGetTranscation.Name = "chbActiveMqGetTranscation";
            this.chbActiveMqGetTranscation.Size = new System.Drawing.Size(48, 16);
            this.chbActiveMqGetTranscation.TabIndex = 9;
            this.chbActiveMqGetTranscation.Text = "事务";
            this.chbActiveMqGetTranscation.UseVisualStyleBackColor = true;
            // 
            // tbActiveMqGetConnectName
            // 
            this.tbActiveMqGetConnectName.Location = new System.Drawing.Point(60, 7);
            this.tbActiveMqGetConnectName.Name = "tbActiveMqGetConnectName";
            this.tbActiveMqGetConnectName.Size = new System.Drawing.Size(127, 21);
            this.tbActiveMqGetConnectName.TabIndex = 8;
            this.tbActiveMqGetConnectName.Text = "tcp://192.101.106.26:61616";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(24, 11);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(29, 12);
            this.label46.TabIndex = 1;
            this.label46.Text = "地址";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.ForeColor = System.Drawing.Color.Red;
            this.label41.Location = new System.Drawing.Point(444, 21);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(17, 12);
            this.label41.TabIndex = 17;
            this.label41.Text = "秒";
            // 
            // tbGetThreadTime
            // 
            this.tbGetThreadTime.Location = new System.Drawing.Point(409, 16);
            this.tbGetThreadTime.Name = "tbGetThreadTime";
            this.tbGetThreadTime.Size = new System.Drawing.Size(32, 21);
            this.tbGetThreadTime.TabIndex = 16;
            this.tbGetThreadTime.Text = "29";
            this.tbGetThreadTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.Color.Red;
            this.label40.Location = new System.Drawing.Point(354, 20);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(53, 12);
            this.label40.TabIndex = 15;
            this.label40.Text = "轮询时间";
            // 
            // pnMsmqGet
            // 
            this.pnMsmqGet.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnMsmqGet.Controls.Add(this.tbMsmqGetEncoding);
            this.pnMsmqGet.Controls.Add(this.label30);
            this.pnMsmqGet.Controls.Add(this.label29);
            this.pnMsmqGet.Controls.Add(this.cbMsmqGetFileType);
            this.pnMsmqGet.Controls.Add(this.chbMsmqGetTranscation);
            this.pnMsmqGet.Controls.Add(this.tbMsmqGetQueueName);
            this.pnMsmqGet.Controls.Add(this.label36);
            this.pnMsmqGet.Location = new System.Drawing.Point(94, 289);
            this.pnMsmqGet.Name = "pnMsmqGet";
            this.pnMsmqGet.Size = new System.Drawing.Size(380, 36);
            this.pnMsmqGet.TabIndex = 14;
            // 
            // tbMsmqGetEncoding
            // 
            this.tbMsmqGetEncoding.Location = new System.Drawing.Point(239, 7);
            this.tbMsmqGetEncoding.Name = "tbMsmqGetEncoding";
            this.tbMsmqGetEncoding.Size = new System.Drawing.Size(44, 21);
            this.tbMsmqGetEncoding.TabIndex = 35;
            this.tbMsmqGetEncoding.Text = "utf-8";
            this.tbMsmqGetEncoding.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(209, 12);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(29, 12);
            this.label30.TabIndex = 34;
            this.label30.Text = "编码";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(289, 12);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(29, 12);
            this.label29.TabIndex = 32;
            this.label29.Text = "格式";
            // 
            // cbMsmqGetFileType
            // 
            this.cbMsmqGetFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMsmqGetFileType.FormattingEnabled = true;
            this.cbMsmqGetFileType.Location = new System.Drawing.Point(318, 8);
            this.cbMsmqGetFileType.Name = "cbMsmqGetFileType";
            this.cbMsmqGetFileType.Size = new System.Drawing.Size(58, 20);
            this.cbMsmqGetFileType.TabIndex = 33;
            // 
            // chbMsmqGetTranscation
            // 
            this.chbMsmqGetTranscation.AutoSize = true;
            this.chbMsmqGetTranscation.Location = new System.Drawing.Point(160, 10);
            this.chbMsmqGetTranscation.Name = "chbMsmqGetTranscation";
            this.chbMsmqGetTranscation.Size = new System.Drawing.Size(48, 16);
            this.chbMsmqGetTranscation.TabIndex = 9;
            this.chbMsmqGetTranscation.Text = "事务";
            this.chbMsmqGetTranscation.UseVisualStyleBackColor = true;
            // 
            // tbMsmqGetQueueName
            // 
            this.tbMsmqGetQueueName.Location = new System.Drawing.Point(60, 8);
            this.tbMsmqGetQueueName.Name = "tbMsmqGetQueueName";
            this.tbMsmqGetQueueName.Size = new System.Drawing.Size(94, 21);
            this.tbMsmqGetQueueName.TabIndex = 8;
            this.tbMsmqGetQueueName.Text = ".\\Private$\\QueueName";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(17, 12);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 12);
            this.label36.TabIndex = 1;
            this.label36.Text = "队列名";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rbActiveMqGet);
            this.panel2.Controls.Add(this.rbMsmqGet);
            this.panel2.Controls.Add(this.rbLocal);
            this.panel2.Controls.Add(this.rbFtpDown);
            this.panel2.Controls.Add(this.rbEmailGet);
            this.panel2.Location = new System.Drawing.Point(7, 72);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(84, 295);
            this.panel2.TabIndex = 13;
            // 
            // rbActiveMqGet
            // 
            this.rbActiveMqGet.AutoSize = true;
            this.rbActiveMqGet.Location = new System.Drawing.Point(6, 268);
            this.rbActiveMqGet.Name = "rbActiveMqGet";
            this.rbActiveMqGet.Size = new System.Drawing.Size(71, 16);
            this.rbActiveMqGet.TabIndex = 6;
            this.rbActiveMqGet.Text = "ActiveMQ";
            this.rbActiveMqGet.UseVisualStyleBackColor = true;
            this.rbActiveMqGet.CheckedChanged += new System.EventHandler(this.rbActiveMqGet_CheckedChanged);
            // 
            // rbMsmqGet
            // 
            this.rbMsmqGet.AutoSize = true;
            this.rbMsmqGet.Location = new System.Drawing.Point(6, 227);
            this.rbMsmqGet.Name = "rbMsmqGet";
            this.rbMsmqGet.Size = new System.Drawing.Size(71, 16);
            this.rbMsmqGet.TabIndex = 5;
            this.rbMsmqGet.Text = "MSMQ接收";
            this.rbMsmqGet.UseVisualStyleBackColor = true;
            this.rbMsmqGet.CheckedChanged += new System.EventHandler(this.rbMsmqGet_CheckedChanged);
            // 
            // rbLocal
            // 
            this.rbLocal.AutoSize = true;
            this.rbLocal.Checked = true;
            this.rbLocal.Location = new System.Drawing.Point(6, 14);
            this.rbLocal.Name = "rbLocal";
            this.rbLocal.Size = new System.Drawing.Size(71, 16);
            this.rbLocal.TabIndex = 0;
            this.rbLocal.TabStop = true;
            this.rbLocal.Text = "本地目录";
            this.rbLocal.UseVisualStyleBackColor = true;
            this.rbLocal.CheckedChanged += new System.EventHandler(this.rbLocal_CheckedChanged);
            // 
            // rbFtpDown
            // 
            this.rbFtpDown.AutoSize = true;
            this.rbFtpDown.Location = new System.Drawing.Point(8, 55);
            this.rbFtpDown.Name = "rbFtpDown";
            this.rbFtpDown.Size = new System.Drawing.Size(65, 16);
            this.rbFtpDown.TabIndex = 3;
            this.rbFtpDown.Text = "FTP下载";
            this.rbFtpDown.UseVisualStyleBackColor = true;
            this.rbFtpDown.CheckedChanged += new System.EventHandler(this.rbFtpDown_CheckedChanged);
            // 
            // rbEmailGet
            // 
            this.rbEmailGet.AutoSize = true;
            this.rbEmailGet.Location = new System.Drawing.Point(7, 154);
            this.rbEmailGet.Name = "rbEmailGet";
            this.rbEmailGet.Size = new System.Drawing.Size(71, 16);
            this.rbEmailGet.TabIndex = 4;
            this.rbEmailGet.Text = "邮件接收";
            this.rbEmailGet.UseVisualStyleBackColor = true;
            this.rbEmailGet.CheckedChanged += new System.EventHandler(this.rbEmailGet_CheckedChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(370, 47);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(53, 12);
            this.label35.TabIndex = 12;
            this.label35.Text = "保存份数";
            // 
            // tbGetSaveTimes
            // 
            this.tbGetSaveTimes.Location = new System.Drawing.Point(426, 43);
            this.tbGetSaveTimes.Name = "tbGetSaveTimes";
            this.tbGetSaveTimes.Size = new System.Drawing.Size(33, 21);
            this.tbGetSaveTimes.TabIndex = 11;
            this.tbGetSaveTimes.Text = "1";
            this.tbGetSaveTimes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnGetRoot
            // 
            this.btnGetRoot.Location = new System.Drawing.Point(309, 41);
            this.btnGetRoot.Name = "btnGetRoot";
            this.btnGetRoot.Size = new System.Drawing.Size(55, 23);
            this.btnGetRoot.TabIndex = 10;
            this.btnGetRoot.Text = "浏览...";
            this.btnGetRoot.UseVisualStyleBackColor = true;
            this.btnGetRoot.Click += new System.EventHandler(this.btnGetRoot_Click);
            // 
            // tbGetMessageRoot
            // 
            this.tbGetMessageRoot.Location = new System.Drawing.Point(94, 43);
            this.tbGetMessageRoot.Name = "tbGetMessageRoot";
            this.tbGetMessageRoot.Size = new System.Drawing.Size(211, 21);
            this.tbGetMessageRoot.TabIndex = 9;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(14, 47);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 12);
            this.label25.TabIndex = 5;
            this.label25.Text = "获取保存目录";
            // 
            // pnEmailRec
            // 
            this.pnEmailRec.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnEmailRec.Controls.Add(this.tbEmailRecUserPass);
            this.pnEmailRec.Controls.Add(this.tbEmailRecUser);
            this.pnEmailRec.Controls.Add(this.label12);
            this.pnEmailRec.Controls.Add(this.label13);
            this.pnEmailRec.Controls.Add(this.tbEmailRecServer);
            this.pnEmailRec.Controls.Add(this.tbEmailRecPort);
            this.pnEmailRec.Controls.Add(this.label10);
            this.pnEmailRec.Controls.Add(this.label11);
            this.pnEmailRec.Location = new System.Drawing.Point(94, 216);
            this.pnEmailRec.Name = "pnEmailRec";
            this.pnEmailRec.Size = new System.Drawing.Size(380, 67);
            this.pnEmailRec.TabIndex = 2;
            // 
            // tbEmailRecUserPass
            // 
            this.tbEmailRecUserPass.Location = new System.Drawing.Point(192, 38);
            this.tbEmailRecUserPass.Name = "tbEmailRecUserPass";
            this.tbEmailRecUserPass.PasswordChar = '*';
            this.tbEmailRecUserPass.Size = new System.Drawing.Size(94, 21);
            this.tbEmailRecUserPass.TabIndex = 19;
            // 
            // tbEmailRecUser
            // 
            this.tbEmailRecUser.Location = new System.Drawing.Point(60, 37);
            this.tbEmailRecUser.Name = "tbEmailRecUser";
            this.tbEmailRecUser.Size = new System.Drawing.Size(94, 21);
            this.tbEmailRecUser.TabIndex = 18;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(160, 42);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "密码";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 16;
            this.label13.Text = "收件人";
            // 
            // tbEmailRecServer
            // 
            this.tbEmailRecServer.Location = new System.Drawing.Point(60, 8);
            this.tbEmailRecServer.Name = "tbEmailRecServer";
            this.tbEmailRecServer.Size = new System.Drawing.Size(94, 21);
            this.tbEmailRecServer.TabIndex = 15;
            // 
            // tbEmailRecPort
            // 
            this.tbEmailRecPort.Location = new System.Drawing.Point(192, 9);
            this.tbEmailRecPort.Name = "tbEmailRecPort";
            this.tbEmailRecPort.Size = new System.Drawing.Size(27, 21);
            this.tbEmailRecPort.TabIndex = 14;
            this.tbEmailRecPort.Text = "110";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(161, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 13;
            this.label10.Text = "端口";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(14, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 12;
            this.label11.Text = "服务器";
            // 
            // pnFtpDown
            // 
            this.pnFtpDown.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnFtpDown.Controls.Add(this.tbFtpDownEncoding);
            this.pnFtpDown.Controls.Add(this.label33);
            this.pnFtpDown.Controls.Add(this.chbFtpDownFileRename);
            this.pnFtpDown.Controls.Add(this.tbFtpDownFileEx);
            this.pnFtpDown.Controls.Add(this.label32);
            this.pnFtpDown.Controls.Add(this.label5);
            this.pnFtpDown.Controls.Add(this.chbFtpDownMode);
            this.pnFtpDown.Controls.Add(this.tbFtpDownUserPass);
            this.pnFtpDown.Controls.Add(this.tbFtpDownUser);
            this.pnFtpDown.Controls.Add(this.tbFtpDownServer);
            this.pnFtpDown.Controls.Add(this.tbFtpDownRoot);
            this.pnFtpDown.Controls.Add(this.tbFtpDownPort);
            this.pnFtpDown.Controls.Add(this.label9);
            this.pnFtpDown.Controls.Add(this.label8);
            this.pnFtpDown.Controls.Add(this.label7);
            this.pnFtpDown.Controls.Add(this.label6);
            this.pnFtpDown.Location = new System.Drawing.Point(94, 114);
            this.pnFtpDown.Name = "pnFtpDown";
            this.pnFtpDown.Size = new System.Drawing.Size(380, 96);
            this.pnFtpDown.TabIndex = 1;
            // 
            // tbFtpDownEncoding
            // 
            this.tbFtpDownEncoding.Location = new System.Drawing.Point(194, 65);
            this.tbFtpDownEncoding.Name = "tbFtpDownEncoding";
            this.tbFtpDownEncoding.Size = new System.Drawing.Size(94, 21);
            this.tbFtpDownEncoding.TabIndex = 20;
            this.tbFtpDownEncoding.Text = "GBK";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(159, 69);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(29, 12);
            this.label33.TabIndex = 19;
            this.label33.Text = "编码";
            // 
            // chbFtpDownFileRename
            // 
            this.chbFtpDownFileRename.AutoSize = true;
            this.chbFtpDownFileRename.Checked = true;
            this.chbFtpDownFileRename.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbFtpDownFileRename.Location = new System.Drawing.Point(294, 40);
            this.chbFtpDownFileRename.Name = "chbFtpDownFileRename";
            this.chbFtpDownFileRename.Size = new System.Drawing.Size(84, 16);
            this.chbFtpDownFileRename.TabIndex = 18;
            this.chbFtpDownFileRename.Text = "文件重命名";
            this.chbFtpDownFileRename.UseVisualStyleBackColor = true;
            // 
            // tbFtpDownFileEx
            // 
            this.tbFtpDownFileEx.Location = new System.Drawing.Point(286, 11);
            this.tbFtpDownFileEx.Name = "tbFtpDownFileEx";
            this.tbFtpDownFileEx.Size = new System.Drawing.Size(78, 21);
            this.tbFtpDownFileEx.TabIndex = 17;
            this.tbFtpDownFileEx.Text = "*.*";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(231, 15);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 12);
            this.label32.TabIndex = 16;
            this.label32.Text = "文件匹配";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 15;
            this.label5.Text = "服务器";
            // 
            // chbFtpDownMode
            // 
            this.chbFtpDownMode.AutoSize = true;
            this.chbFtpDownMode.Location = new System.Drawing.Point(294, 68);
            this.chbFtpDownMode.Name = "chbFtpDownMode";
            this.chbFtpDownMode.Size = new System.Drawing.Size(72, 16);
            this.chbFtpDownMode.TabIndex = 14;
            this.chbFtpDownMode.Text = "主动模式";
            this.chbFtpDownMode.UseVisualStyleBackColor = true;
            // 
            // tbFtpDownUserPass
            // 
            this.tbFtpDownUserPass.Location = new System.Drawing.Point(194, 38);
            this.tbFtpDownUserPass.Name = "tbFtpDownUserPass";
            this.tbFtpDownUserPass.PasswordChar = '*';
            this.tbFtpDownUserPass.Size = new System.Drawing.Size(94, 21);
            this.tbFtpDownUserPass.TabIndex = 13;
            // 
            // tbFtpDownUser
            // 
            this.tbFtpDownUser.Location = new System.Drawing.Point(58, 38);
            this.tbFtpDownUser.Name = "tbFtpDownUser";
            this.tbFtpDownUser.Size = new System.Drawing.Size(94, 21);
            this.tbFtpDownUser.TabIndex = 12;
            // 
            // tbFtpDownServer
            // 
            this.tbFtpDownServer.Location = new System.Drawing.Point(59, 11);
            this.tbFtpDownServer.Name = "tbFtpDownServer";
            this.tbFtpDownServer.Size = new System.Drawing.Size(94, 21);
            this.tbFtpDownServer.TabIndex = 11;
            // 
            // tbFtpDownRoot
            // 
            this.tbFtpDownRoot.Location = new System.Drawing.Point(58, 65);
            this.tbFtpDownRoot.Name = "tbFtpDownRoot";
            this.tbFtpDownRoot.Size = new System.Drawing.Size(94, 21);
            this.tbFtpDownRoot.TabIndex = 10;
            // 
            // tbFtpDownPort
            // 
            this.tbFtpDownPort.Location = new System.Drawing.Point(194, 12);
            this.tbFtpDownPort.Name = "tbFtpDownPort";
            this.tbFtpDownPort.Size = new System.Drawing.Size(27, 21);
            this.tbFtpDownPort.TabIndex = 9;
            this.tbFtpDownPort.Text = "21";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 12);
            this.label9.TabIndex = 6;
            this.label9.Text = "获取目录";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(159, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 5;
            this.label8.Text = "密码";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "用户";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(160, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 3;
            this.label6.Text = "端口";
            // 
            // pnLocal
            // 
            this.pnLocal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnLocal.Controls.Add(this.tbLoaclRootFileEx);
            this.pnLocal.Controls.Add(this.label38);
            this.pnLocal.Controls.Add(this.btnRootView);
            this.pnLocal.Controls.Add(this.tbLoaclRoot);
            this.pnLocal.Controls.Add(this.label4);
            this.pnLocal.Location = new System.Drawing.Point(94, 72);
            this.pnLocal.Name = "pnLocal";
            this.pnLocal.Size = new System.Drawing.Size(380, 36);
            this.pnLocal.TabIndex = 0;
            // 
            // tbLoaclRootFileEx
            // 
            this.tbLoaclRootFileEx.Location = new System.Drawing.Point(286, 7);
            this.tbLoaclRootFileEx.Name = "tbLoaclRootFileEx";
            this.tbLoaclRootFileEx.Size = new System.Drawing.Size(78, 21);
            this.tbLoaclRootFileEx.TabIndex = 11;
            this.tbLoaclRootFileEx.Text = "*.*";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(230, 11);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(53, 12);
            this.label38.TabIndex = 10;
            this.label38.Text = "文件匹配";
            // 
            // btnRootView
            // 
            this.btnRootView.Location = new System.Drawing.Point(160, 5);
            this.btnRootView.Name = "btnRootView";
            this.btnRootView.Size = new System.Drawing.Size(56, 23);
            this.btnRootView.TabIndex = 9;
            this.btnRootView.Text = "浏览...";
            this.btnRootView.UseVisualStyleBackColor = true;
            this.btnRootView.Click += new System.EventHandler(this.btnRootView_Click);
            // 
            // tbLoaclRoot
            // 
            this.tbLoaclRoot.Location = new System.Drawing.Point(60, 7);
            this.tbLoaclRoot.Name = "tbLoaclRoot";
            this.tbLoaclRoot.Size = new System.Drawing.Size(94, 21);
            this.tbLoaclRoot.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "获取目录";
            // 
            // gbSend
            // 
            this.gbSend.Controls.Add(this.pnActiveMqSend);
            this.gbSend.Controls.Add(this.label44);
            this.gbSend.Controls.Add(this.tbSendThreadTime);
            this.gbSend.Controls.Add(this.label45);
            this.gbSend.Controls.Add(this.tbSendFileEx);
            this.gbSend.Controls.Add(this.label39);
            this.gbSend.Controls.Add(this.pnMsmqSend);
            this.gbSend.Controls.Add(this.tbSendCorpNo);
            this.gbSend.Controls.Add(this.label27);
            this.gbSend.Controls.Add(this.btnSendRoot);
            this.gbSend.Controls.Add(this.tbSendMessageRoot);
            this.gbSend.Controls.Add(this.btnSendStart);
            this.gbSend.Controls.Add(this.label26);
            this.gbSend.Controls.Add(this.panel1);
            this.gbSend.Controls.Add(this.label2);
            this.gbSend.Controls.Add(this.pnEmailSend);
            this.gbSend.Controls.Add(this.pnFtpUpload);
            this.gbSend.Controls.Add(this.btnSendStop);
            this.gbSend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbSend.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbSend.Location = new System.Drawing.Point(3, 3);
            this.gbSend.Name = "gbSend";
            this.gbSend.Size = new System.Drawing.Size(483, 374);
            this.gbSend.TabIndex = 7;
            this.gbSend.TabStop = false;
            // 
            // pnActiveMqSend
            // 
            this.pnActiveMqSend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnActiveMqSend.Controls.Add(this.tbActiveMqSendDestinationName);
            this.pnActiveMqSend.Controls.Add(this.label48);
            this.pnActiveMqSend.Controls.Add(this.chbActiveMqSendTranscation);
            this.pnActiveMqSend.Controls.Add(this.tbActiveMqSendConnectName);
            this.pnActiveMqSend.Controls.Add(this.label49);
            this.pnActiveMqSend.Location = new System.Drawing.Point(94, 332);
            this.pnActiveMqSend.Name = "pnActiveMqSend";
            this.pnActiveMqSend.Size = new System.Drawing.Size(380, 36);
            this.pnActiveMqSend.TabIndex = 41;
            // 
            // tbActiveMqSendDestinationName
            // 
            this.tbActiveMqSendDestinationName.Location = new System.Drawing.Point(235, 7);
            this.tbActiveMqSendDestinationName.Name = "tbActiveMqSendDestinationName";
            this.tbActiveMqSendDestinationName.Size = new System.Drawing.Size(75, 21);
            this.tbActiveMqSendDestinationName.TabIndex = 11;
            this.tbActiveMqSendDestinationName.Text = "queue://test";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(192, 12);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(41, 12);
            this.label48.TabIndex = 10;
            this.label48.Text = "连接名";
            // 
            // chbActiveMqSendTranscation
            // 
            this.chbActiveMqSendTranscation.AutoSize = true;
            this.chbActiveMqSendTranscation.Checked = true;
            this.chbActiveMqSendTranscation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbActiveMqSendTranscation.Location = new System.Drawing.Point(323, 10);
            this.chbActiveMqSendTranscation.Name = "chbActiveMqSendTranscation";
            this.chbActiveMqSendTranscation.Size = new System.Drawing.Size(48, 16);
            this.chbActiveMqSendTranscation.TabIndex = 9;
            this.chbActiveMqSendTranscation.Text = "事务";
            this.chbActiveMqSendTranscation.UseVisualStyleBackColor = true;
            // 
            // tbActiveMqSendConnectName
            // 
            this.tbActiveMqSendConnectName.Location = new System.Drawing.Point(60, 7);
            this.tbActiveMqSendConnectName.Name = "tbActiveMqSendConnectName";
            this.tbActiveMqSendConnectName.Size = new System.Drawing.Size(127, 21);
            this.tbActiveMqSendConnectName.TabIndex = 8;
            this.tbActiveMqSendConnectName.Text = "tcp://192.101.106.26:61616";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(23, 11);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(29, 12);
            this.label49.TabIndex = 1;
            this.label49.Text = "地址";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.Color.Red;
            this.label44.Location = new System.Drawing.Point(444, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(17, 12);
            this.label44.TabIndex = 40;
            this.label44.Text = "秒";
            // 
            // tbSendThreadTime
            // 
            this.tbSendThreadTime.Location = new System.Drawing.Point(409, 16);
            this.tbSendThreadTime.Name = "tbSendThreadTime";
            this.tbSendThreadTime.Size = new System.Drawing.Size(32, 21);
            this.tbSendThreadTime.TabIndex = 39;
            this.tbSendThreadTime.Text = "31";
            this.tbSendThreadTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.ForeColor = System.Drawing.Color.Red;
            this.label45.Location = new System.Drawing.Point(354, 20);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(53, 12);
            this.label45.TabIndex = 38;
            this.label45.Text = "轮询时间";
            // 
            // tbSendFileEx
            // 
            this.tbSendFileEx.Location = new System.Drawing.Point(380, 44);
            this.tbSendFileEx.Name = "tbSendFileEx";
            this.tbSendFileEx.Size = new System.Drawing.Size(79, 21);
            this.tbSendFileEx.TabIndex = 37;
            this.tbSendFileEx.Text = "*.*";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(322, 50);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(53, 12);
            this.label39.TabIndex = 36;
            this.label39.Text = "文件匹配";
            // 
            // pnMsmqSend
            // 
            this.pnMsmqSend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnMsmqSend.Controls.Add(this.tbMsmqSendEncoding);
            this.pnMsmqSend.Controls.Add(this.label31);
            this.pnMsmqSend.Controls.Add(this.label28);
            this.pnMsmqSend.Controls.Add(this.cbMsmqSendFileType);
            this.pnMsmqSend.Controls.Add(this.chbMsmqSendTranscation);
            this.pnMsmqSend.Controls.Add(this.tbMsmqSendQueueName);
            this.pnMsmqSend.Controls.Add(this.label37);
            this.pnMsmqSend.Location = new System.Drawing.Point(94, 287);
            this.pnMsmqSend.Name = "pnMsmqSend";
            this.pnMsmqSend.Size = new System.Drawing.Size(380, 36);
            this.pnMsmqSend.TabIndex = 35;
            // 
            // tbMsmqSendEncoding
            // 
            this.tbMsmqSendEncoding.Location = new System.Drawing.Point(239, 7);
            this.tbMsmqSendEncoding.Name = "tbMsmqSendEncoding";
            this.tbMsmqSendEncoding.Size = new System.Drawing.Size(44, 21);
            this.tbMsmqSendEncoding.TabIndex = 37;
            this.tbMsmqSendEncoding.Text = "utf-8";
            this.tbMsmqSendEncoding.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(209, 12);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(29, 12);
            this.label31.TabIndex = 36;
            this.label31.Text = "编码";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(288, 12);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(29, 12);
            this.label28.TabIndex = 11;
            this.label28.Text = "格式";
            // 
            // cbMsmqSendFileType
            // 
            this.cbMsmqSendFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMsmqSendFileType.FormattingEnabled = true;
            this.cbMsmqSendFileType.Location = new System.Drawing.Point(318, 7);
            this.cbMsmqSendFileType.Name = "cbMsmqSendFileType";
            this.cbMsmqSendFileType.Size = new System.Drawing.Size(58, 20);
            this.cbMsmqSendFileType.TabIndex = 31;
            // 
            // chbMsmqSendTranscation
            // 
            this.chbMsmqSendTranscation.AutoSize = true;
            this.chbMsmqSendTranscation.Checked = true;
            this.chbMsmqSendTranscation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbMsmqSendTranscation.Location = new System.Drawing.Point(161, 9);
            this.chbMsmqSendTranscation.Name = "chbMsmqSendTranscation";
            this.chbMsmqSendTranscation.Size = new System.Drawing.Size(48, 16);
            this.chbMsmqSendTranscation.TabIndex = 9;
            this.chbMsmqSendTranscation.Text = "事务";
            this.chbMsmqSendTranscation.UseVisualStyleBackColor = true;
            // 
            // tbMsmqSendQueueName
            // 
            this.tbMsmqSendQueueName.Location = new System.Drawing.Point(60, 7);
            this.tbMsmqSendQueueName.Name = "tbMsmqSendQueueName";
            this.tbMsmqSendQueueName.Size = new System.Drawing.Size(94, 21);
            this.tbMsmqSendQueueName.TabIndex = 8;
            this.tbMsmqSendQueueName.Text = "FormatName:DIRECT=TCP:192.168.6.9\\Private$\\QueueName";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(12, 11);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 12);
            this.label37.TabIndex = 1;
            this.label37.Text = "队列名";
            // 
            // tbSendCorpNo
            // 
            this.tbSendCorpNo.Location = new System.Drawing.Point(94, 17);
            this.tbSendCorpNo.Name = "tbSendCorpNo";
            this.tbSendCorpNo.Size = new System.Drawing.Size(101, 21);
            this.tbSendCorpNo.TabIndex = 9;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(34, 21);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(57, 12);
            this.label27.TabIndex = 8;
            this.label27.Text = "任务代码";
            // 
            // btnSendRoot
            // 
            this.btnSendRoot.Location = new System.Drawing.Point(254, 45);
            this.btnSendRoot.Name = "btnSendRoot";
            this.btnSendRoot.Size = new System.Drawing.Size(55, 23);
            this.btnSendRoot.TabIndex = 34;
            this.btnSendRoot.Text = "浏览...";
            this.btnSendRoot.UseVisualStyleBackColor = true;
            this.btnSendRoot.Click += new System.EventHandler(this.btnSendRoot_Click);
            // 
            // tbSendMessageRoot
            // 
            this.tbSendMessageRoot.Location = new System.Drawing.Point(94, 46);
            this.tbSendMessageRoot.Name = "tbSendMessageRoot";
            this.tbSendMessageRoot.Size = new System.Drawing.Size(150, 21);
            this.tbSendMessageRoot.TabIndex = 33;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(14, 50);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 12);
            this.label26.TabIndex = 32;
            this.label26.Text = "本地发送目录";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbActiveMqSend);
            this.panel1.Controls.Add(this.rbMsmqSend);
            this.panel1.Controls.Add(this.rbEmailSend);
            this.panel1.Controls.Add(this.rbFtpUpload);
            this.panel1.Location = new System.Drawing.Point(7, 77);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(84, 292);
            this.panel1.TabIndex = 31;
            // 
            // rbActiveMqSend
            // 
            this.rbActiveMqSend.AutoSize = true;
            this.rbActiveMqSend.Location = new System.Drawing.Point(6, 265);
            this.rbActiveMqSend.Name = "rbActiveMqSend";
            this.rbActiveMqSend.Size = new System.Drawing.Size(71, 16);
            this.rbActiveMqSend.TabIndex = 8;
            this.rbActiveMqSend.Text = "ActiveMQ";
            this.rbActiveMqSend.UseVisualStyleBackColor = true;
            this.rbActiveMqSend.CheckedChanged += new System.EventHandler(this.rbActiveMqSend_CheckedChanged);
            // 
            // rbMsmqSend
            // 
            this.rbMsmqSend.AutoSize = true;
            this.rbMsmqSend.Location = new System.Drawing.Point(6, 221);
            this.rbMsmqSend.Name = "rbMsmqSend";
            this.rbMsmqSend.Size = new System.Drawing.Size(71, 16);
            this.rbMsmqSend.TabIndex = 7;
            this.rbMsmqSend.Text = "MSMQ发送";
            this.rbMsmqSend.UseVisualStyleBackColor = true;
            this.rbMsmqSend.CheckedChanged += new System.EventHandler(this.rbMsmqSend_CheckedChanged);
            // 
            // rbEmailSend
            // 
            this.rbEmailSend.AutoSize = true;
            this.rbEmailSend.Location = new System.Drawing.Point(7, 115);
            this.rbEmailSend.Name = "rbEmailSend";
            this.rbEmailSend.Size = new System.Drawing.Size(71, 16);
            this.rbEmailSend.TabIndex = 6;
            this.rbEmailSend.Text = "邮件发送";
            this.rbEmailSend.UseVisualStyleBackColor = true;
            this.rbEmailSend.CheckedChanged += new System.EventHandler(this.rbEmailSend_CheckedChanged);
            // 
            // rbFtpUpload
            // 
            this.rbFtpUpload.AutoSize = true;
            this.rbFtpUpload.Checked = true;
            this.rbFtpUpload.Location = new System.Drawing.Point(6, 12);
            this.rbFtpUpload.Name = "rbFtpUpload";
            this.rbFtpUpload.Size = new System.Drawing.Size(65, 16);
            this.rbFtpUpload.TabIndex = 5;
            this.rbFtpUpload.TabStop = true;
            this.rbFtpUpload.Text = "FTP上传";
            this.rbFtpUpload.UseVisualStyleBackColor = true;
            this.rbFtpUpload.CheckedChanged += new System.EventHandler(this.rbFtpUpload_CheckedChanged);
            // 
            // pnEmailSend
            // 
            this.pnEmailSend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnEmailSend.Controls.Add(this.label24);
            this.pnEmailSend.Controls.Add(this.tbEmailSendTo);
            this.pnEmailSend.Controls.Add(this.label23);
            this.pnEmailSend.Controls.Add(this.tbEmailSendUserPass);
            this.pnEmailSend.Controls.Add(this.tbEmailSendUser);
            this.pnEmailSend.Controls.Add(this.label19);
            this.pnEmailSend.Controls.Add(this.label20);
            this.pnEmailSend.Controls.Add(this.tbEmailSendServer);
            this.pnEmailSend.Controls.Add(this.tbEmailSendPort);
            this.pnEmailSend.Controls.Add(this.label21);
            this.pnEmailSend.Controls.Add(this.label22);
            this.pnEmailSend.Location = new System.Drawing.Point(94, 179);
            this.pnEmailSend.Name = "pnEmailSend";
            this.pnEmailSend.Size = new System.Drawing.Size(380, 98);
            this.pnEmailSend.TabIndex = 4;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.label24.Location = new System.Drawing.Point(292, 71);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(65, 12);
            this.label24.TabIndex = 30;
            this.label24.Text = "以分号隔开";
            // 
            // tbEmailSendTo
            // 
            this.tbEmailSendTo.Location = new System.Drawing.Point(60, 67);
            this.tbEmailSendTo.Name = "tbEmailSendTo";
            this.tbEmailSendTo.Size = new System.Drawing.Size(226, 21);
            this.tbEmailSendTo.TabIndex = 29;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(12, 71);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 12);
            this.label23.TabIndex = 28;
            this.label23.Text = "收件人";
            // 
            // tbEmailSendUserPass
            // 
            this.tbEmailSendUserPass.Location = new System.Drawing.Point(190, 39);
            this.tbEmailSendUserPass.Name = "tbEmailSendUserPass";
            this.tbEmailSendUserPass.PasswordChar = '*';
            this.tbEmailSendUserPass.Size = new System.Drawing.Size(96, 21);
            this.tbEmailSendUserPass.TabIndex = 27;
            // 
            // tbEmailSendUser
            // 
            this.tbEmailSendUser.Location = new System.Drawing.Point(58, 38);
            this.tbEmailSendUser.Name = "tbEmailSendUser";
            this.tbEmailSendUser.Size = new System.Drawing.Size(94, 21);
            this.tbEmailSendUser.TabIndex = 26;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(158, 43);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 12);
            this.label19.TabIndex = 25;
            this.label19.Text = "密码";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(13, 43);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 12);
            this.label20.TabIndex = 24;
            this.label20.Text = "发件人";
            // 
            // tbEmailSendServer
            // 
            this.tbEmailSendServer.Location = new System.Drawing.Point(60, 9);
            this.tbEmailSendServer.Name = "tbEmailSendServer";
            this.tbEmailSendServer.Size = new System.Drawing.Size(92, 21);
            this.tbEmailSendServer.TabIndex = 23;
            // 
            // tbEmailSendPort
            // 
            this.tbEmailSendPort.Location = new System.Drawing.Point(190, 10);
            this.tbEmailSendPort.Name = "tbEmailSendPort";
            this.tbEmailSendPort.Size = new System.Drawing.Size(27, 21);
            this.tbEmailSendPort.TabIndex = 22;
            this.tbEmailSendPort.Text = "25";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(159, 15);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 12);
            this.label21.TabIndex = 21;
            this.label21.Text = "端口";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(12, 14);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 12);
            this.label22.TabIndex = 20;
            this.label22.Text = "服务器";
            // 
            // pnFtpUpload
            // 
            this.pnFtpUpload.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnFtpUpload.Controls.Add(this.tbFtpUploadEncoding);
            this.pnFtpUpload.Controls.Add(this.label34);
            this.pnFtpUpload.Controls.Add(this.label14);
            this.pnFtpUpload.Controls.Add(this.chbFtpUploadMode);
            this.pnFtpUpload.Controls.Add(this.tbFtpUploadUserPass);
            this.pnFtpUpload.Controls.Add(this.tbFtpUploadUser);
            this.pnFtpUpload.Controls.Add(this.tbFtpUploadServer);
            this.pnFtpUpload.Controls.Add(this.tbFtpUploadRoot);
            this.pnFtpUpload.Controls.Add(this.tbFtpUploadPort);
            this.pnFtpUpload.Controls.Add(this.label15);
            this.pnFtpUpload.Controls.Add(this.label16);
            this.pnFtpUpload.Controls.Add(this.label17);
            this.pnFtpUpload.Controls.Add(this.label18);
            this.pnFtpUpload.Location = new System.Drawing.Point(94, 76);
            this.pnFtpUpload.Name = "pnFtpUpload";
            this.pnFtpUpload.Size = new System.Drawing.Size(380, 97);
            this.pnFtpUpload.TabIndex = 3;
            // 
            // tbFtpUploadEncoding
            // 
            this.tbFtpUploadEncoding.Location = new System.Drawing.Point(191, 66);
            this.tbFtpUploadEncoding.Name = "tbFtpUploadEncoding";
            this.tbFtpUploadEncoding.Size = new System.Drawing.Size(94, 21);
            this.tbFtpUploadEncoding.TabIndex = 28;
            this.tbFtpUploadEncoding.Text = "GBK";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(159, 70);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(29, 12);
            this.label34.TabIndex = 27;
            this.label34.Text = "编码";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 14);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 26;
            this.label14.Text = "服务器";
            // 
            // chbFtpUploadMode
            // 
            this.chbFtpUploadMode.AutoSize = true;
            this.chbFtpUploadMode.Location = new System.Drawing.Point(299, 66);
            this.chbFtpUploadMode.Name = "chbFtpUploadMode";
            this.chbFtpUploadMode.Size = new System.Drawing.Size(72, 16);
            this.chbFtpUploadMode.TabIndex = 25;
            this.chbFtpUploadMode.Text = "主动模式";
            this.chbFtpUploadMode.UseVisualStyleBackColor = true;
            // 
            // tbFtpUploadUserPass
            // 
            this.tbFtpUploadUserPass.Location = new System.Drawing.Point(191, 39);
            this.tbFtpUploadUserPass.Name = "tbFtpUploadUserPass";
            this.tbFtpUploadUserPass.PasswordChar = '*';
            this.tbFtpUploadUserPass.Size = new System.Drawing.Size(94, 21);
            this.tbFtpUploadUserPass.TabIndex = 24;
            // 
            // tbFtpUploadUser
            // 
            this.tbFtpUploadUser.Location = new System.Drawing.Point(60, 38);
            this.tbFtpUploadUser.Name = "tbFtpUploadUser";
            this.tbFtpUploadUser.Size = new System.Drawing.Size(93, 21);
            this.tbFtpUploadUser.TabIndex = 23;
            // 
            // tbFtpUploadServer
            // 
            this.tbFtpUploadServer.Location = new System.Drawing.Point(60, 10);
            this.tbFtpUploadServer.Name = "tbFtpUploadServer";
            this.tbFtpUploadServer.Size = new System.Drawing.Size(93, 21);
            this.tbFtpUploadServer.TabIndex = 22;
            // 
            // tbFtpUploadRoot
            // 
            this.tbFtpUploadRoot.Location = new System.Drawing.Point(58, 66);
            this.tbFtpUploadRoot.Name = "tbFtpUploadRoot";
            this.tbFtpUploadRoot.Size = new System.Drawing.Size(96, 21);
            this.tbFtpUploadRoot.TabIndex = 21;
            // 
            // tbFtpUploadPort
            // 
            this.tbFtpUploadPort.Location = new System.Drawing.Point(191, 11);
            this.tbFtpUploadPort.Name = "tbFtpUploadPort";
            this.tbFtpUploadPort.Size = new System.Drawing.Size(27, 21);
            this.tbFtpUploadPort.TabIndex = 20;
            this.tbFtpUploadPort.Text = "21";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(24, 71);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(29, 12);
            this.label15.TabIndex = 19;
            this.label15.Text = "目录";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(159, 43);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(29, 12);
            this.label16.TabIndex = 18;
            this.label16.Text = "密码";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(25, 43);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 17;
            this.label17.Text = "用户";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(160, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 12);
            this.label18.TabIndex = 16;
            this.label18.Text = "端口";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnCancel);
            this.groupBox2.Controls.Add(this.btnSave);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Location = new System.Drawing.Point(0, 405);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(497, 44);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(261, 15);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "取消";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(148, 15);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // folderBrowser
            // 
            this.folderBrowser.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tpGet);
            this.tabControl.Controls.Add(this.tpSend);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(497, 405);
            this.tabControl.TabIndex = 9;
            // 
            // tpGet
            // 
            this.tpGet.Controls.Add(this.gbGet);
            this.tpGet.Location = new System.Drawing.Point(4, 21);
            this.tpGet.Name = "tpGet";
            this.tpGet.Padding = new System.Windows.Forms.Padding(3);
            this.tpGet.Size = new System.Drawing.Size(489, 380);
            this.tpGet.TabIndex = 0;
            this.tpGet.Text = "获取设置";
            this.tpGet.UseVisualStyleBackColor = true;
            // 
            // tpSend
            // 
            this.tpSend.Controls.Add(this.gbSend);
            this.tpSend.Location = new System.Drawing.Point(4, 21);
            this.tpSend.Name = "tpSend";
            this.tpSend.Padding = new System.Windows.Forms.Padding(3);
            this.tpSend.Size = new System.Drawing.Size(489, 380);
            this.tpSend.TabIndex = 1;
            this.tpSend.Text = "发送设置";
            this.tpSend.UseVisualStyleBackColor = true;
            // 
            // TaskForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 449);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "TaskForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "任务设置";
            this.Load += new System.EventHandler(this.TaskForm_Load);
            this.gbGet.ResumeLayout(false);
            this.gbGet.PerformLayout();
            this.pnActiveMqGet.ResumeLayout(false);
            this.pnActiveMqGet.PerformLayout();
            this.pnMsmqGet.ResumeLayout(false);
            this.pnMsmqGet.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.pnEmailRec.ResumeLayout(false);
            this.pnEmailRec.PerformLayout();
            this.pnFtpDown.ResumeLayout(false);
            this.pnFtpDown.PerformLayout();
            this.pnLocal.ResumeLayout(false);
            this.pnLocal.PerformLayout();
            this.gbSend.ResumeLayout(false);
            this.gbSend.PerformLayout();
            this.pnActiveMqSend.ResumeLayout(false);
            this.pnActiveMqSend.PerformLayout();
            this.pnMsmqSend.ResumeLayout(false);
            this.pnMsmqSend.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnEmailSend.ResumeLayout(false);
            this.pnEmailSend.PerformLayout();
            this.pnFtpUpload.ResumeLayout(false);
            this.pnFtpUpload.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tpGet.ResumeLayout(false);
            this.tpSend.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbGet;
        private System.Windows.Forms.GroupBox gbSend;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel pnEmailRec;
        private System.Windows.Forms.Panel pnFtpDown;
        private System.Windows.Forms.Panel pnLocal;
        private System.Windows.Forms.Panel pnEmailSend;
        private System.Windows.Forms.Panel pnFtpUpload;
        private System.Windows.Forms.RadioButton rbEmailGet;
        private System.Windows.Forms.RadioButton rbFtpDown;
        private System.Windows.Forms.RadioButton rbLocal;
        private System.Windows.Forms.RadioButton rbEmailSend;
        private System.Windows.Forms.RadioButton rbFtpUpload;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSendStop;
        private System.Windows.Forms.Button btnSendStart;
        private System.Windows.Forms.Button btnGetStop;
        private System.Windows.Forms.Button btnGetStart;
        private System.Windows.Forms.TextBox tbTaskNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbLoaclRoot;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnRootView;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbFtpDownPort;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chbFtpDownMode;
        private System.Windows.Forms.TextBox tbFtpDownUserPass;
        private System.Windows.Forms.TextBox tbFtpDownUser;
        private System.Windows.Forms.TextBox tbFtpDownServer;
        private System.Windows.Forms.TextBox tbFtpDownRoot;
        private System.Windows.Forms.TextBox tbEmailRecServer;
        private System.Windows.Forms.TextBox tbEmailRecPort;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbEmailRecUserPass;
        private System.Windows.Forms.TextBox tbEmailRecUser;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbEmailSendUserPass;
        private System.Windows.Forms.TextBox tbEmailSendUser;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox tbEmailSendServer;
        private System.Windows.Forms.TextBox tbEmailSendPort;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chbFtpUploadMode;
        private System.Windows.Forms.TextBox tbFtpUploadUserPass;
        private System.Windows.Forms.TextBox tbFtpUploadUser;
        private System.Windows.Forms.TextBox tbFtpUploadServer;
        private System.Windows.Forms.TextBox tbFtpUploadRoot;
        private System.Windows.Forms.TextBox tbFtpUploadPort;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbEmailSendTo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbGetMessageRoot;
        private System.Windows.Forms.TextBox tbSendMessageRoot;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnGetRoot;
        private System.Windows.Forms.Button btnSendRoot;
        private System.Windows.Forms.FolderBrowserDialog fbdGetRoot;
        private System.Windows.Forms.FolderBrowserDialog fdbSendRoot;
        private System.Windows.Forms.TextBox tbSendCorpNo;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.FolderBrowserDialog fbdTransferFrom;
        private System.Windows.Forms.FolderBrowserDialog fbdTransferTo;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tbGetSaveTimes;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnMsmqGet;
        private System.Windows.Forms.TextBox tbMsmqGetQueueName;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.RadioButton rbMsmqGet;
        private System.Windows.Forms.CheckBox chbMsmqGetTranscation;
        private System.Windows.Forms.Panel pnMsmqSend;
        private System.Windows.Forms.CheckBox chbMsmqSendTranscation;
        private System.Windows.Forms.TextBox tbMsmqSendQueueName;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.RadioButton rbMsmqSend;
        private System.Windows.Forms.TextBox tbLoaclRootFileEx;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox tbSendFileEx;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox tbGetThreadTime;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox tbSendThreadTime;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel pnActiveMqGet;
        private System.Windows.Forms.TextBox tbActiveMqGetDestinationName;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.CheckBox chbActiveMqGetTranscation;
        private System.Windows.Forms.TextBox tbActiveMqGetConnectName;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.RadioButton rbActiveMqGet;
        private System.Windows.Forms.Panel pnActiveMqSend;
        private System.Windows.Forms.TextBox tbActiveMqSendDestinationName;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.CheckBox chbActiveMqSendTranscation;
        private System.Windows.Forms.TextBox tbActiveMqSendConnectName;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.RadioButton rbActiveMqSend;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cbMsmqSendFileType;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cbMsmqGetFileType;
        private System.Windows.Forms.TextBox tbMsmqGetEncoding;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox tbMsmqSendEncoding;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpGet;
        private System.Windows.Forms.TabPage tpSend;
        private System.Windows.Forms.TextBox tbFtpDownFileEx;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox chbFtpDownFileRename;
        private System.Windows.Forms.TextBox tbFtpDownEncoding;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tbFtpUploadEncoding;
        private System.Windows.Forms.Label label34;
    }
}
