using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;

namespace FileTransferService
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private void serviceProcessInstaller1_BeforeInstall(object sender, InstallEventArgs e)
        {
            this.serviceInstaller.DisplayName = Config.SystemSet.AppServiceName;
            this.serviceInstaller.Description = Config.SystemSet.AppServiceDesc;
        }
    }
}
